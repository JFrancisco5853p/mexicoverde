import { Injectable } from '@angular/core';
import { contacto } from '../models/contacto';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {
  public contacto: contacto;
  public blockForm: boolean;
  public esOther: boolean;
  public esAdmin: boolean;
  constructor() {
    this.contacto = new contacto();
    this.blockForm = false;
    this.esOther = false;
    this.esAdmin = false;
  };

  editarInfo(data: any){
    localStorage.setItem('Usuario', JSON.stringify({nombre: data.nombre, telefono: data.telefono, correo: data.correo}));
  };

  obtenerUsuario(){
    return localStorage.getItem('Usuario');
  };

  eliminarUsuario(){
    localStorage.removeItem('Usuario');
  };
}
