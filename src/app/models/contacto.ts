export class contacto {
  public nombre: string;
  public telefono: string;
  public correo: string;

  constructor(
  ){
    this.nombre = '';
    this.telefono = '';
    this.correo = '';
    this.crearUsuario();
  }

  crearUsuario(){
    if (localStorage.getItem('Usuario') === null) {//Usuario por defecto
      this.nombre = 'Juan Francisco Sobrevilla Salvador';
      this.telefono = '8461107832';
      this.correo = 'franciscosobrevilla97@gmail.com';
      localStorage.setItem('Usuario', JSON.stringify({ nombre: this.nombre, telefono: this.telefono, correo: this.correo }));
    } else {//obtiene el usuario editado
      let contactoLocalStorage = JSON.parse(localStorage.getItem("Usuario") ?? "[]");
      console.log(contactoLocalStorage);
      this.nombre = contactoLocalStorage.nombre;
      this.telefono = contactoLocalStorage.telefono;
      this.correo = contactoLocalStorage.correo;
    }
  };

}
