import { Component, Input, DoCheck  } from '@angular/core';
import { contacto } from 'src/app/models/contacto';
import { ContactoService } from 'src/app/services/contacto.service';
import { MensajesService } from 'src/app/services/mensajes.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-pagetwo',
  templateUrl: './pagetwo.component.html',
  styleUrls: ['./pagetwo.component.css']
})
export class PagetwoComponent implements DoCheck {
  public contacto: contacto;
  public myForm: FormGroup;

  constructor(
    public contacService: ContactoService,
    private msj: MensajesService
  ){
    this.contacto = new contacto();
    /*Se crea formulario*/
    this.myForm = new FormGroup({
      nombre: new FormControl(this.contacto.nombre),
      telefono: new FormControl(this.contacto.telefono),
      correo: new FormControl(this.contacto.correo)
    });
  }

  Editar(data:any){
    if (data.value.nombre === null && data.value.telefono === null && data.value.correo === null) {
      this.msj.mensajeError('Error', 'Imposible editar registro vacio, recarga la página...');
    } else {
      this.contacService.editarInfo(data.value);
      this.msj.mensajeCorrecto('Correcto', 'Se edito correctameente');
    }
  };

  Eliminar(data:any){
    this.myForm.reset();
    this.contacService.eliminarUsuario();
    this.msj.mensajeCorrecto('Correcto', 'Se borro correctamente');
  };

  ngDoCheck(){//Evento del ciclo de vida que obtiene los cambios de alguna accion dentro de la app de angular
    if (this.contacService.esAdmin){
      this.myForm.enable();
    } else if (this.contacService.esOther){
      this.myForm.disable();
    }
  }
}
