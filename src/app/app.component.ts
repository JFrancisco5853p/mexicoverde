import { Component } from '@angular/core';
import { ContactoService } from './services/contacto.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public mostrarPage: boolean;//bandera para mostrar u ocultar opciones del header

  constructor(
    private contacService: ContactoService
  ){
    this.mostrarPage = true;
    if (localStorage.getItem('mPage') === null){//Si no existe el registro en la base de datos
      localStorage.setItem('mPage', 'true');
    } else {//Toma el valor que se encuentra ya en la base de datos
      this.mostrarPage = localStorage.getItem('mPage') === "true" ? true: false;
    }
  }

  infoPage1(){
    this.mostrarPage = true;
    localStorage.setItem('mPage', 'true');
  };

  infoPage2(){
    this.mostrarPage = false;
    localStorage.setItem('mPage', 'false');
  };

  adminClick(){//evitar
    this.contacService.esAdmin = true;
    this.contacService.esOther = false;
  }

  otherClick(){
    this.contacService.esAdmin = false;
    this.contacService.esOther = true;
  }

}
