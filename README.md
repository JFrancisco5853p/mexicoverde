
## appBlogMexicoVerde
Blog Application

## Starting
The project aims to visualize the great landscapes that exist in the nature of Mexico.
To start installing the project on your desktop, do the following: Install the project!

The project adapts to any screen (desktop, tablet and mobile).

## Name
Application for Blog.

## Description
None.

## Facility
Load the project in a local environment, do the following:

Install nodejs, I recommend the LTS version which is more stable.
Install AngularCLI.

At the end to run the project execute the following commands:

## install npm
`ng serve`


ready...
More components that were used for the database and better visualization were:
LocalStorage, used to store data locally in the browser.
Bootstrap, for a better visualization of components.

## Support
franciscosobrevilla97@gmail.com

## Use
none.

## Project status
none
